#pragma once

namespace hooks {
	void AI_RemoveFromAttack();
}
constexpr int ATTACK_LIMIT = 64;
